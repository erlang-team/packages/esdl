esdl (1.3.1-4) unstable; urgency=medium

  * Fix a minor bug with listing video modes.
  * Change the old Alioth VCS headers to the new Salsa ones.
  * Fix the package homepage to a working one.
  * Bump the debhelper compatibility level to 10.
  * Bump the standards version to 4.1.3.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 23 Mar 2018 13:37:56 +0300

esdl (1.3.1-3) unstable; urgency=medium

  * Fixed FTBFS on GNU/kFreebsd and GNU/Hurd because of rebar config options
    which were enabled for Linux only.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 03 Apr 2016 10:15:29 +0300

esdl (1.3.1-2) unstable; urgency=medium

  * Fixed FTBFS when only architecture dependent packages are to be built.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 03 Apr 2016 08:28:36 +0300

esdl (1.3.1-1) unstable; urgency=low

  * New upstream release.
  * Switched to 3.0 (quilt) source format and bumped the debhelper
    compatibility version to 9.
  * Switched to rebar for building the package because it's the only
    supported build mechanism now.
  * Bumped standards version to 3.9.7.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 02 Apr 2016 23:21:26 +0300

esdl (1.2-2) unstable; urgency=low

  * Upload to unstable.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 13 Jan 2012 10:15:15 +0400

esdl (1.2-1) experimental; urgency=low

  * New upstream release.
  * Moved the installed files back to /usr/lib/erlang/lib tree because their
    conflicts with main Erlang distribution are resolved.
  * Do not include gl.hrl in sdl_img.erl and sdl_ttf.erl as it is not necessary
    and breaks their build.
  * Replaced -fpic by -fPIC in c_src/Makefile.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 23 Dec 2011 22:22:47 +0400

esdl (1.0.1-4) experimental; urgency=low

  * Fixed typo in package description (closes: #601194).
  * Bumped standards version to 3.9.2.
  * Fixed loading driver esdl_driver into Erlang R15B.
  * Upload to experimental as Erlang R15B is there yet.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Dec 2011 12:45:07 +0400

esdl (1.0.1-3) unstable; urgency=low

  * Changed maintainer's email address to the mailing list
    pkg-erlang-devel@lists.alioth.debian.org.
  * Changed VCS headers to svn://svn.debian.org/.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Oct 2009 15:18:48 +0400

esdl (1.0.1-2) unstable; urgency=low

  * Removed Torsten Werner from the uploaders list as per his request.
  * Added README.source where it is explained how to apply a series of patches
    to a pristine source.
  * Overridden lintian warning about wirtual package dependency without real
    package dependency because it's intentional and warns for breakage
    on Erlang updates.
  * Bumped standards version to 3.8.3.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Oct 2009 14:33:23 +0400

esdl (1.0.1-1) unstable; urgency=low

  * New upstream release.
  * Added Klas Johansson to debian/copyright as an author of SDL_img and
    SDL_ttf bindings.
  * Removed tail call patch because the bug is fixed upstream.
  * Removed hack for arm architecture because this architecture is no longer
    in Debian.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 23 May 2009 18:10:19 +0400

esdl (1.0-1) unstable; urgency=low

  * New upstream release.
  * Enabled newly implemented SDL_ttf (TrueType fonts) and SDL_image (loading
    images as SDL surfaces) support.
  * Renamed source package to esdl to match upstream name.
  * Renamed binary package to erlang-esdl as this name better fits Erlang
    modules naming scheme, introduced in erlang since 1:13.b.
  * Removed sources from the binary package and split out Erlang headers
    and documentation into separate packages erlang-esdl-dev and
    erlang-esdl-doc.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 17 May 2009 16:10:32 +0400

libsdl-erlang (0.96.0626-13) unstable; urgency=low

  * Upload to unstable after Erlang R13B is uploaded to sid.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 01 May 2009 13:26:01 +0400

libsdl-erlang (0.96.0626-12) experimental; urgency=low

  * Removed unnecessary package grep-dctrl from build dependencies.
  * Added build dependency on version 1:13.b of erlang-dev to simplify
    transition to Erlang R13B.
  * Replaced all instances of erlang:fault/1 (which is removed from Erlang
    in version R13) by erlang:error/1.
  * Moved installed binaries from /usr/lib/erlang/lib hierarchy to
    /usr/lib/esdl directory because modules gl and glu conflict with wxErlang
    which is included into Erlang since version R13A. This means that to use
    esdl ona has to run Erlang emulator with -pa /usr/lib/esdl/ebin option
    and cannot use wxErlang at the same time. Documented this option in
    README.Debian.
  * Bumped standards version to 3.8.1.
  * Instructed uscan control file debian/watch to use SourceForge redirector.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 22 Apr 2009 21:04:35 +0400

libsdl-erlang (0.96.0626-11) unstable; urgency=low

  * Redefined HOME environment variable to fix FTBFS in case when HOME points
    to an existing inaccessible directory.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 18 Jul 2008 13:21:06 +0400

libsdl-erlang (0.96.0626-10) unstable; urgency=low

  * Reordered calls to dh_makeshlibs, dh_shlibdeps, dh_installdeb in
    debian/rules.
  * Removed obsolete packages (xlibmesa-gl-dev and xlibmesa-glu-dev) from
    build-dependencies.
  * Bumped standards version to 3.8.0.
  * Added Vcs-Svn and Vcs-Browser headers to debian/control.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 18 Jul 2008 11:38:46 +0400

libsdl-erlang (0.96.0626-9) unstable; urgency=low

  * Removed perl from build dependencies because it is build-essential.
  * Added a patch which replaces deprecated tail +2 call by tail -n +2.
  * Added a hack to bypass GCC internal compiler error on arm architecture.
  * Use quilt for patch management.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 25 May 2008 18:43:38 +0400

libsdl-erlang (0.96.0626-8) unstable; urgency=low

  * Added homepage header to debian/control.
  * Bumped standards version to 3.7.3.
  * Fixed doc-base registration file (fixed typo Abtract -> Abstract and
    section name capitalization programming -> Programming).
  * Clarified copyright statement.
  * Lowered erlang version in build-dependencies to make porting the package
    to etch easier.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 25 May 2008 14:35:13 +0400

libsdl-erlang (0.96.0626-7) unstable; urgency=low

  * Added Sergei Golovan <sgolovan@debian.org> to uploaders list.
  * Rewritten clean target in debian/rules to ignore only missing Makefile
    error.
  * Bumped debhelper compatibility level to 5.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 06 Sep 2007 21:23:05 +0400

libsdl-erlang (0.96.0626-6) unstable; urgency=low

  [ Sergei Golovan ]
  * Added dependency on erlang ABI virtual package.
  * Added build-indep and build-arch targets to debain/rules.

 -- Torsten Werner <twerner@debian.org>  Tue, 26 Jun 2007 18:57:19 +0400

libsdl-erlang (0.96.0626-5) unstable; urgency=low

  * Switched to erlang-depends.

 -- Torsten Werner <twerner@debian.org>  Wed, 11 Apr 2007 19:19:12 +0400

libsdl-erlang (0.96.0626-4) unstable; urgency=low

  [ Sergei Golovan ]
  * Added author name and packaging information to debian/copyright.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Thu, 14 Sep 2006 20:32:03 +0400

libsdl-erlang (0.96.0626-3) unstable; urgency=low

  [ Torsten Werner ]
  * Made Build-Depends compatible with sarge, etch, and ubuntu.

  [ Sergei Golovan ]
  * Added versioned erlang dependency.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Wed, 09 Aug 2006 23:27:56 +0400

libsdl-erlang (0.96.0626-2) unstable; urgency=low

  [ Sergei Golovan ]
  * Upload to unstable.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Wed, 19 Jul 2006 09:48:34 +0400

libsdl-erlang (0.96.0626-1) experimental; urgency=low

  [ Sergei Golovan ]
  * New upstream version.
  * Added debian/watch.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Tue, 11 Jul 2006 21:44:59 +0400

libsdl-erlang (0.95.0630-2) unstable; urgency=low

  * New maintainer (Closes:  #368073).

  [ Sergei Golovan ]
  * Bumped standards version to 3.7.2.
  * Incorporated non-maintainer upload changes from 0.95.0630-1.1
    (Closes: #366723, #368357).
  * Changed Build-Depends and Depends to reflect erlang 10.b.10
    package structure.
  * Added get-orig-source target to debian/rules.

 -- Erlang Packagers <erlang-pkg-devel@lists.berlios.de>  Mon, 10 Jul 2006 23:22:12 +0400

libsdl-erlang (0.95.0630-1.1) unstable; urgency=low

  * Non-Maintainer Upload.
  * Build-depend on libglu1-mesa-dev instead of xlibmesa-glu-dev. 
    (Closes: #366723)

 -- Frederik Schüler <fs@debian.org>  Sat, 20 May 2006 17:38:41 +0000

libsdl-erlang (0.95.0630-1) unstable; urgency=low

  * New upstream version.
  * Bumped standards version to 3.6.2.

 -- Will Newton <will@debian.org>  Wed,  6 Jul 2005 13:37:49 +0100

libsdl-erlang (0.94.1025-1) unstable; urgency=low

  * New maintainer.
  * New upstream version.
  * Package removed from sarge. (Closes: #305995)
  * Moved from interpreters to libs section.
  * Updated standards version.
  * Removed non-functional postinst and redundant README.Debian. 

 -- Will Newton <will@debian.org>  Thu, 19 May 2005 17:18:25 +0100

libsdl-erlang (0.94.0615-1) unstable; urgency=low

  * New upstream release.

 -- Brent A. Fulgham <bfulgham@debian.org>  Mon,  9 Aug 2004 23:43:31 -0700

libsdl-erlang (0.94.0125-1) unstable; urgency=low

  * New upstream release.

 -- Brent A. Fulgham <bfulgham@debian.org>  Sat,  7 Feb 2004 21:31:13 -0800

libsdl-erlang (0.93.0526-1) unstable; urgency=low

  * New upstream release

 -- Brent A. Fulgham <bfulgham@debian.org>  Mon, 23 Jun 2003 23:09:20 -0700

libsdl-erlang (0.93.0314-1) unstable; urgency=low

  * New upstream release.
  * Fragment_program_arb added as an extension.
  * Erldemo program now sports bump mapping.

 -- Brent A. Fulgham <bfulgham@debian.org>  Fri, 16 May 2003 23:10:23 -0700

libsdl-erlang (0.93.0131-2) unstable; urgency=low

  * Correction to postinst.

 -- Brent A. Fulgham <bfulgham@debian.org>  Sat,  8 Feb 2003 23:59:42 -0800

libsdl-erlang (0.93.0131-1) unstable; urgency=low

  * New upstream release
  * Put erlang files in versioned directory, per Erlang standards.

 -- Brent A. Fulgham <bfulgham@debian.org>  Sat,  8 Feb 2003 23:11:07 -0800

libsdl-erlang (0.91.1009-9) unstable; urgency=low

  * Added build-depends on xlibmesa-glu-dev for autobuilders.

 -- Brent A. Fulgham <bfulgham@debian.org>  Sat,  8 Feb 2003 00:01:06 -0800

libsdl-erlang (0.91.1009-8) unstable; urgency=low

  * Close the final lintian error.

 -- Brent A. Fulgham <bfulgham@debian.org>  Thu, 30 Jan 2003 21:19:03 -0800

libsdl-erlang (0.91.1009-7) unstable; urgency=low

  * Correct several lintian errors.

 -- Brent A. Fulgham <bfulgham@debian.org>  Tue, 28 Jan 2003 21:45:03 -0800

libsdl-erlang (0.91.1009-6) unstable; urgency=low

  * Correct build-depends to be Build-Depends-Indep.

 -- Brent A. Fulgham <bfulgham@debian.org>  Tue, 28 Jan 2003 21:32:25 -0800

libsdl-erlang (0.91.1009-5) unstable; urgency=low

  * Add build-dependency on xlibmesa-gl-dev so GL dependencies are met and
    autobuilders can build package.

 -- Brent A. Fulgham <bfulgham@debian.org>  Mon, 27 Jan 2003 23:32:21 -0800

libsdl-erlang (0.91.1009-4) unstable; urgency=low

  * Correct doc-base path, so menu entry takes you to correct documentation.

 -- Brent A. Fulgham <bfulgham@debian.org>  Sat, 14 Dec 2002 23:01:10 -0800

libsdl-erlang (0.91.1009-3) unstable; urgency=low

  * Add Erlang to build-depends.  Whoops!

 -- Brent A. Fulgham <bfulgham@debian.org>  Thu, 14 Nov 2002 22:01:46 -0800

libsdl-erlang (0.91.1009-2) unstable; urgency=low

  * Include 'src' files so Wings3D will build.

 -- Brent A. Fulgham <bfulgham@debian.org>  Sun, 10 Nov 2002 00:12:56 -0800

libsdl-erlang (0.91.1009-1) unstable; urgency=low

  * New upstream release.

 -- Brent A. Fulgham <bfulgham@debian.org>  Thu,  7 Nov 2002 23:28:51 -0800
